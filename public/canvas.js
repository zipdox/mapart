function clearCanvas(canvas){
    canvas.width = canvas.height = 128;
    const ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, 128, 128);
}

function sizeCanvas(canvas, width, height){
    canvas.width = 128*width;
    canvas.height = 128*height;
    return true;
}