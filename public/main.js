window.onload = async function(){

    const mapJson = await fetch('maps.json');
    window.mapData = await mapJson.json();
    
    window.mapslist = document.getElementById('maptable');
    window.canvas = document.getElementById('map');
    window.maptitle = document.getElementById('maptitle');
    window.mapAuthor = document.getElementById('author');
    window.searchBar = document.getElementById('search');
    window.reverseSearch = document.getElementById('reversesearch');
    window.reverseSearchBtn = document.getElementById('reversesearchbtn');

    window.sortByName = document.getElementById('sortByName');
    window.sortByAuthors = document.getElementById('sortByAuthors');
    window.sortBySize = document.getElementById('sortBySize');

    var invertSort = {name: false, authors: false, size: false};
    function sortBy(maps){
        sortedMaps = [...maps];
        updateTableHeaders();
        switch(sortParameter){
            case 'name':
                if(!invertSort.name){
                    sortedMaps.sort((a, b) => a.name.localeCompare(b.name));
                }else{
                    sortedMaps.sort((a, b) => -a.name.localeCompare(b.name));
                }
                break;
            case 'authors':
                if(!invertSort.authors){
                    sortedMaps.sort((a, b) => a.authors.join(', ').localeCompare(b.authors.join(', ')));
                }else{
                    sortedMaps.sort((a, b) => -a.authors.join(', ').localeCompare(b.authors.join(', ')));
                }
                break;
            case 'size':
                if(!invertSort.size){
                    sortedMaps.sort((a, b) => (a.width * a.height > b.width * b.height) ? 1 : -1);
                }else{
                    sortedMaps.sort((a, b) => (a.width * a.height < b.width * b.height) ? 1 : -1);
                }
                break;
            default:
                return;
        }
        return sortedMaps;
    }

    function loadTable(maps){
        mapslist.innerHTML = '';
        for(let map of maps){
            const entry = createEntry(map);
            mapslist.appendChild(entry);
        }
    }

    window.sortParameter = 'name';
    window.currentMaps = sortBy(mapData);
    invertSort.name = !invertSort.name;

    function updateTableHeaders(){
        sortByName.textContent = 'Title';
        sortByAuthors.textContent = 'Authors';
        sortBySize.textContent = 'Size';
        switch(sortParameter){
            case 'name':
                if(!invertSort.name) sortByName.textContent = 'Title↓'
                else sortByName.textContent = 'Title↑';
                break;
            case 'authors':
                if(!invertSort.authors) sortByAuthors.textContent = 'Authors↓'
                else sortByAuthors.textContent = 'Authors↑';
                break;
            case 'size':
                if(!invertSort.size) sortBySize.textContent = 'Size↓'
                else sortBySize.textContent = 'Size↑';
                break;
            default:
                break;
        }
    }

    loadTable(currentMaps);

    sortByName.onclick = function(){
        if(sortParameter != 'name') invertSort.name = false;
        sortParameter = 'name';
        loadTable(sortBy(currentMaps));
        invertSort.name = !invertSort.name;
    }
    sortByAuthors.onclick = function(){
        if(sortParameter != 'authors') invertSort.authors = false;
        sortParameter = 'authors';
        loadTable(sortBy(currentMaps));
        invertSort.authors = !invertSort.authors;
    }
    sortBySize.onclick = function(){
        if(sortParameter != 'size') invertSort.size = false;
        sortParameter = 'size';
        loadTable(sortBy(currentMaps));
        invertSort.size = !invertSort.size;
    }

    const searchWorker = new Worker('/search.js');
    searchWorker.postMessage({maps: window.mapData});
    searchWorker.onmessage = function(event){
        mapslist.innerHTML = '';
        window.currentMaps = event.data;
        loadTable(sortBy(window.currentMaps));
    }
    searchBar.oninput = function(){
        if(window.searchTimeout) clearTimeout(searchTimeout);
        window.searchTimeout = setTimeout(function(){
            delete window.searchTimeout;
            searchWorker.postMessage({search: searchBar.value});
        }, 100);
    }
    
    function downloadAtScale(canvas, scale){
        const helperCanvas = document.createElement('canvas');
        helperCanvas.width = canvas.width * scale;
        helperCanvas.height = canvas.height * scale;
        const helperCtx = helperCanvas.getContext('2d');
        helperCtx.imageSmoothingEnabled = false;
        helperCtx.drawImage(canvas, 0, 0, canvas.width * scale, canvas.height * scale);
        const downloadLink = document.createElement('a');
        downloadLink.href = helperCanvas.toDataURL("image/png");
        downloadLink.download = window.maptitle.textContent + '.png';
        downloadLink.click();
        downloadLink.remove();
    }

    canvas.onclick = function(){
        const scale = prompt('Download map scale', 1);
        if(!scale || scale == NaN) return;
        downloadAtScale(this, scale);
    }

    const urlID = window.location.hash.slice(1);
    if(urlID.length > 0){
        decodedUrlID = decodeURIComponent(urlID);
        var foundMap;
        for(let map of mapData){
            if(map.name == decodedUrlID){
                foundMap = map;
                break;
            }
        }

        if(foundMap == undefined) return;

        window.shownMap = foundMap;
        maptitle.innerHTML = foundMap.name;
        mapAuthor.textContent = 'By ' + foundMap.authors.join(', ');
        sizeCanvas(window.canvas, foundMap.width, foundMap.height);
        drawMap(foundMap);
    }

    const nbt = require('prismarine-nbt');
    // I fucking hate having to use Node libraries but I'm too lazy to write my own NBT implementation
    const { Buffer } = require('buffer');

    function parseNbt(buf){
        return new Promise((resolve, reject)=>{
            nbt.parse(Buffer.from(buf), function(error, data) {
                if (error) return reject(error);
                resolve(data);
            });
        });
    }

    function compareMaps(map1, map2){
        if(map1.length != 16384 || map2.length != 16384) return 0;
        let match = 0
        for(let i = 0; i < 16384; i++){
            if(map1[i] == map2[i]) match++;
        }
        return match / 16384;
    }

    reverseSearchBtn.onclick = ()=>{
        reverseSearch.click();
    }

    reverseSearch.onchange = async function(){
        reverseSearchBtn.disabled = true;
        const file = this.files[0];
        const fileData = await file.arrayBuffer();

        let rawData;
        if(file.name.endsWith('.dat')){
            let nbt = await parseNbt(fileData).catch(err=>{
                alert(err);
            });
            if(!nbt){
                reverseSearchBtn.disabled = false;
                return;
            }
            rawData = nbt?.value?.data?.value?.colors?.value;
        }else{
            rawData = fileData;
        }
        let imageData = new Uint8ClampedArray(rawData);

        let progress = 0;
        let mapFound = false;
        let bestMatch = false;
        let bestMatchScore = 0;
        for(let map of mapData){
            for(let tile of map.maps){
                let tileData = await getMapData(tile.id);
                let match = compareMaps(tileData, imageData)
                if(match > bestMatchScore){
                    bestMatchScore = match;
                    bestMatch = map;
                }
                if(match == 1){
                    mapFound = true;
                    break;
                }
            }
            if(mapFound) break;
            let percentProgress = Math.floor(progress / mapData.length * 100) + '%';
            reverseSearchBtn.textContent = percentProgress;
            progress++;
        }
        reverseSearchBtn.textContent = 'Reverse search';
        reverseSearchBtn.disabled = false;
        if(mapFound){
            alert(`Map found: ${bestMatch.name} by ${bestMatch.authors.join(', ')}.`);
        }else if(bestMatch){
            alert(`Best match at ${Math.floor(bestMatchScore * 100)}%: ${bestMatch.name} by ${bestMatch.authors.join(', ')}.`);
        }else{
            alert("No match found");
        }
    }
}
